var act = '';
$('#msg_wrapper').css({
  'height': screen.height * 0.8 + 'px'
});
$('#user_s').click(function () {
  ($('#user_s').width() == 250) ? $('#l_out').hide() : $('#l_out').show();
  $('#user_s').animate({
    'width': ($('#user_s').width() == 250) ? 50 : 250
  }, 100);
  $('#user_list').delay(300).slideToggle();
});
var unread = Array();
var del = Array();
var del_msg = Array();

function del_list(x) {
  var tmp = $(x).children("input[name='id']").val();
  if (del.indexOf(tmp) == -1) {
    $(x).addClass('del_list');
    if (del.length == 0) $('#del').css('display', 'block');
    del.push(tmp);
    del_msg.push(x);
  } else {
    $(x).removeClass('del_list');
    del_msg.splice(del.indexOf(tmp), 1);
    del.splice(del.indexOf(tmp), 1);
    if (del.length == 0) $('#del').css('display', 'none');
  }
}

function dlt() {
  $.ajax({
    url: 'php/msg.php',
    type: 'POST',
    data: {
      data: del,
      cnf: 5
    },
    success: function (data) {
      for (var i = 0; i < del_msg.length; i++)
      $(del_msg[i]).remove();
      del = [];
      del_msg = [];
      $('#del').css('display', 'none');
    }
  });
}
for (var i = 0; i < $('.to').length; i++) {
  var tmp = $('.to').eq(i).children('.to_n').html();
  if (!(tmp in unread)) {
    unread.push(tmp);
    unread[tmp] = {
      q: 0,
      obj: $('.to').eq(i).children('.to_c')
    }
  }
}

function update() {
  $.ajax({
    type: 'POST',
    url: 'php/msg.php',
    data: {
      cnf: 4,
      name: $('.unread').length == 0 ? '' : act
    },
    success: function (data) {
      $('.to_c').html('');
      var tlt = 'Tulip';
      //alert(data);
      if (data) data = JSON.parse(data);
      if (data['n'] == '0') {
        document.title = 'Tulip';
        $('.msg').removeClass('unread');
      }
      if (data['list']) {
        data = data['list'];
        for (var i = 0; i < data.length; i++) {
          for (var key in data[i]) {
            if (!(key in unread)) {
              var a = document.createElement('div');
              $('#alias').append(a);
              $(a).prop('class', 'to');
              var str = "<div class='to_c'>+" + data[i][key] + "</div><div class='to_n' onclick='load(this.innerHTML, 0)'>" + key + "</div>";
              $(a).html(str);
              unread[key] = {
                q: data[i][key],
                obj: a
              }
            } else {
              if ($(unread[key].obj).html() != data[i][key]) {
                $(unread[key].obj).html('+' + data[i][key]);
                if (act == key) tlt = key + ': ' + '+' + data[i][key] + ' messages';
              }
            }
          }
        }
      }
      document.title = tlt;
    }
  });
}
update();

function resize(obj) {
  $("body").append("<div id='bg'></div>");
  var el = new Image();
  $(el).attr("src", $(obj).attr("src"));
  $(el).attr("class", "temp");
  $("#bg").append(el);
  var w, h;
  if ($(el).width() / $(el).height() > screen.width / screen.height) {
    w = $("#bg").width() * 0.9;
    $(el).width(w);
    h = $(el).height();
  } else {
    h = $("#bg").height() * 0.9;
    $(el).height(h);
    w = $(el).width();
  }
  $(el).css({
    top: obj.offsetTop - window.scrollY,
    left: obj.offsetLeft - window.scrollX,
    width: $(obj).width(),
    height: $(obj).height()
  });
  $("#bg").animate({
    opacity: 1
  }, 1000)
  $(el).animate({
    top: "50%",
    marginTop: -h / 2,
    marginLeft: -w / 2,
    left: "50%",
    width: w,
    height: h
  }, 1000);
  $(el).click(function () {
    $(el).animate({
      top: obj.offsetTop - window.scrollY,
      left: obj.offsetLeft - window.scrollX,
      width: $(obj).width(),
      height: $(obj).height(),
      marginTop: 0,
      marginLeft: 0
    }, 1000).queue(function () {
      $("#bg").animate({
        opacity: 0,
      }, 100).queue(function () {
        $(el).remove();
        $("#bg").remove();
      });
    })
  })
}

function popup(x) {
  $("#main").append("<div class='popup'>" + x + "</div>");
  $(".popup").css({
    marginTop: -$(".popup").height() / 2,
    marginLeft: -$(".popup").width() / 2
  });
  $(".popup").click(function () {
    $(this).remove();
  });
  $(".popup").delay(4000).animate({
    opacity: 0
  }, 500).queue(function () {
    $(this).remove();
  });
}
var trd = Array();

function load(x, c) {
  var noload = 0;
  if (c != 1 & act != '') $('#l_bg').css('display', 'block');
  var act_ = act;
  if (x != act & act != '' & c == 0 & x in trd) {
    trd[act].html = $('#msg_box').html();
    noload = 1;
    //alert(noload);
  }
  if (!(x in trd)) {
    noload = 0;
    trd[x] = {
      data: [],
      html: '',
      s: -1,
      f: -1,
      a: 15,
      t_: '',
      name: x,
      status: 0
    }
  } else noload = (c == 1 | c == 3) ? 0 : 1;
  $.ajax({
    type: 'POST',
    url: 'php/msg.php',
    data: {
      cnf: (noload == 0) ? ((c == 0 | c == 3) ? 0 : 1) : -1,
      data: {
        s: trd[x].s,
        f: trd[x].f,
        name: trd[x].name
      }
    },
    success: function (data) {
      if (data) data = JSON.parse(data)
      else return;
      var elem = $('#msg_wrapper')[0];
      var all_scr = (c == 0 | (elem.scrollHeight - elem.scrollTop < screen.height) & data.status == '1' & c == 1);
      trd[x].s = data.s;
      trd[x].f = data.f;
      trd[x].a = data.a;
      trd[x].status = data.status;
      if (c != 1) $('#l_bg').css('display', 'none');
      if ((act_ == x | act_ == '') & act == act_) {
        for (key in data.data) {
          var msg = document.createElement('div');
          $(msg).addClass('msg');
          $(msg).click(function () {
            del_list(this)
          });
          $(msg).addClass(data.data[key].class);
          if (data.data[key].read == '0') $(msg).addClass('unread');
          var html = "<input value='" + data.data[key].id + "' name='id' disabled='true'>";
          html += "<div class='msg_n'>" + data.data[key].name + "</div>";
          html += "<div class='msg_d'>" + data.data[key].date + "</div><br />";
          html += "<div class='msg_t'>" + data.data[key].time + "</div>";
          html += "<div class='clr'></div>";
          html += "<div class='msg_b'>" + data.data[key].text + "</div>";
          if (c == 0 | c == 3) {
            $(msg).prepend(html);
            $('#msg_box').prepend(msg);
            if ($(msg).next().children("input[name='id']").val() == $(msg).children("input[name='id']").val()) {
              alert('fail prevented');
              $(msg).remove();
            }
            trd[x].data.unshift(data.data[key]);
          } else {
            $(msg).append(html);
            $('#msg_box').append(msg);
            if ($(msg).prev().children("input[name='id']").val() == $(msg).children("input[name='id']").val()) {
              alert('fail prevented');
              $(msg).remove();
            }
            var temp_r = 20 * (data.data[key].class == 'dst' ? 1 : -1);
            $(msg).css({
              '-webkit-transform': 'rotate(' + temp_r + 'deg)',
              '-moz-transform': 'rotate(' + temp_r + 'deg)',
              'transform': 'rotate(' + temp_r + 'deg)',
              width: 40 + '%'
            });
            $(msg).animate({
              borderSpacing: temp_r
            }, {
              step: function (now, fx) {
                $(this).css('-webkit-transform', 'rotate(' + (now - temp_r) + 'deg)');
                $(this).css('-moz-transform', 'rotate(' + (now - temp_r) + 'deg)');
                $(this).css('transform', 'rotate(' + (now - temp_r) + 'deg)');
              },
              queue: false,
              duration: 'slow'
            }, 'linear');
            $(msg).animate({
              width: '100%'
            });
            trd[x].data.push(data.data[key]);
          }
        }
      } else if (act_ == act) {
        $('#msg_box').html('');
        for (key in data.data) {
          trd[x].data.unshift(data.data[key]);
        }
        for (key in trd[x].data) {
          var msg = document.createElement('div');
          $(msg).click(function () {
            del_list(this)
          });
          $(msg).addClass('msg');
          $(msg).addClass(trd[x].data[key].class);
          if (trd[x].data[key].read == '0') $(msg).addClass('unread');
          var html = "<input value='" + trd[x].data[key].id + "' name='id' disabled='true'>";
          html += "<div class='msg_n'>" + trd[x].data[key].name + "</div>";
          html += "<div class='msg_d'>" + trd[x].data[key].date + "</div><br />";
          html += "<div class='msg_t'>" + trd[x].data[key].time + "</div>";
          html += "<div class='clr'></div>";
          html += "<div class='msg_b'>" + trd[x].data[key].text + "</div>";
          $(msg).prepend(html);
          if ($(msg).next().children("input[name='id']").val() == $(msg).children("input[name='id']").val()) {
            $(msg).remove();
            alert('fail prevented');
          }
          $(msg).css({
            marginLeft: 200 * (trd[x].data[key].class == 'dst' ? 1 : -1)
          });
          $('#msg_box').append(msg);
          $(msg).animate({
            marginLeft: 0
          }, 300);
        }
      }
      if (c == 0) act = x;
      if (all_scr) {
        elem.scrollTop = elem.scrollHeight;
      }
    }
  });
}
$('#msg_wrapper').scroll(function () {
  if ($(this)[0].scrollTop < 100) {
    console.log(act);
    load(act, 3);
  }
});
$('textarea[name=msg_inp]').focus(function (e) {
  e.preventDefault();
});

function send() {
  var d = {
    dst: act,
    text: $('textarea[name=msg_inp]').val(),
    status: ''
  }
  $('textarea[name=msg_inp]').val('');
  $.ajax({
    type: 'POST',
    url: 'php/msg.php',
    data: {
      data: d,
      cnf: 2
    },
    success: function (data) {
      var elem = $('#msg_wrapper')[0];
      elem.scrollTop = elem.scrollHeight;
    }
  });
}

var t = setInterval(function () {
  update();
  if (act != '') load(act, 1);
}, 1000);

function read() {
  $.ajax({
    type: 'POST',
    url: 'php/msg.php',
    data: {
      cnf: 3,
      name: act
    },
    success: function (data) {
      for (var i = 0; i < $('.msg').length; i++) {
        if ($('.msg').eq(i).children('.msg_n').html() == act) $('.msg').eq(i).removeClass('unread');
      }
      /*for (var i = 0; i < trd[act].data.length; i++) {
        if (trd[act].data[i].children('.msg_n').html() == act) 
          trd[act].data[i].removeClass('unread');
      }*/
    }
  });
}

function enter(e) {
  var key = e.keyCode || e.charCode || e.which;
  read();
  if (key == 13) {
    e.preventDefault();
    send();
  }
}

function img_hover(x, e) {
  e.preventDefault();
  $("#text").append("<div id='hover_div'>Нажмите, чтобы рассмотреть</div>");
  window.onmousemove = function (e) {
    $("#hover_div").css({
      position: "absolute",
      top: e.pageY + 20,
      left: e.pageX + 20,
      cursor: "-webkit-zoom-in",
      cursor: "-moz-zoom-in",
      background: "white",
      opacity: .7,
      padding: 5
    });
  }
  x.onmouseout = function () {
    $("#hover_div").remove();
  }
}
var p_o, i_o;

function mng(x, obj, e, err_b, chk, dsbl) {
  var str1 = $(obj).val();
  var str2 = $(chk).val();
  var ch = e.keyCode || e.which || e.charCode;
  $.ajax({
    type: "POST",
    url: "php/mng.php",
    data: {
      ch_c: ch,
      s1: str1,
      s2: str2,
      cnf: x
    },
    success: function (data) {
      if (x == 0) i_o = false;
      if (x == 1) p_o = false;
      $(dsbl).prop('disabled', true);
      $(err_b).css('background', '#FA6A57');
      switch (parseInt(data)) {
      case 0:
        if (x == 0) i_o = true;
        if (x == 1) p_o = true;
        $(err_b).html('ok');
        $(err_b).css('background', '#95FFC7');
        if (p_o == true & i_o == true) $(dsbl).prop('disabled', false);
        break;
      case 1:
        $(err_b).html('');
        break;
      case 2:
        $(err_b).html('You can use latin symbols only');
        break;
      case 3:
        $(err_b).html("User name alredy exists");
        break;
      case 4:
        $(err_b).html("passwords don't match");
        break;
      case 5:
        $(err_b).html("password's too short");
        break;
      default:
        break;
      }
    }
  });
}