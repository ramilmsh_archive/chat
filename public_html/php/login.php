<?php
include('core/User.class.php');
if (isset($_SESSION['name'])) {
  echo "<script>window.location.href = 'exp.inedutech.ru/chat_'</script>";
} else {
  if (isset($_POST['login'])) {
    $log_name = str_replace("'", "\'", str_replace('"', '\"', $_POST['log_name']));
    $log_pass = str_replace("'", "\'", str_replace('"', '\"', $_POST['log_pass']));
  	$user = new User($log_name, md5($log_pass));
    //$_SESSION['debug'] = "$_POST[log_name] - $_POST[log_pass]<br>" . $_SESSION['debug'];
    $user->login();
  }
  if (isset($_POST['register'])) {
    $reg_name = str_replace("'", "\'", str_replace('"', '\"', $_POST['reg_name']));
    $reg_pass = str_replace("'", "\'", str_replace('"', '\"', $_POST['reg_pass']));
  	$user = new User($reg_name, md5($reg_pass));
    //$_SESSION['debug'] = "$_POST[reg_name] - $_POST[reg_pass]<br>" . $_SESSION['debug'];
    $user->register();
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>
      Tulip | Log in
    </title>
    <link type='text/css' href='css/style.css' rel='stylesheet'>
  </head>
  <body>
    <div id='log_wrapper'>
      <form method='post' action=''>
        <input name='log_name' placeholder='Login' type='text' class='text_input'><br>
        <input name='log_pass' placeholder='Password' type='password' class='text_input'><br>
        <input name='login' value='Log in' type='submit' class='btn' id='lg_btn'>
      </form>
      <div onclick="$('#reg_wrapper').show(); $('#log_wrapper').hide()" style='cursor:pointer'>Register</div>
    </div>    
    <div id='reg_wrapper' style='display:none'>
      <form method='post' action=''>
        <input name='reg_name' placeholder='Login' type='text' class='text_input' onkeyup="mng(0, this, event, $('#err1'), null, $('#rg_btn'))"><div id='err1' class='err'></div>
        <input name='reg_pass' placeholder='Password' type='password' class='text_input' onkeyup="mng(1, this, event, $('#err2'), $('input[name=repass]'), $('#rg_btn'))">
        <input name='repass' placeholder='Password' type='password' class='text_input' onkeyup="mng(1, this, event, $('#err2'), $('input[name=reg_pass]'), $('#rg_btn'))"><div id='err2' class='err'></div>
        <input name='register' value='register' type='submit' class='btn' id='rg_btn' disabled>
      </form>
      <div onclick="$('#reg_wrapper').hide(); $('#log_wrapper').show()" style='cursor:pointer'>Log in</div>
    </div>
    <script src='js/jq-lib.js'></script>
    <script src='js/script.js'></script>
  </body>
</html>