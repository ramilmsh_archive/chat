<!DOCTYPE html>
<html>
  <head>
    <title>
      Tulip
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type='text/css' href='css/style.css' rel='stylesheet'>
    <?php
include("core/SQL.class.php");
      if (isset($_POST['log_out'])) {
        unset($_SESSION['name']);
        unset($_SESSION['password']);
        echo "<script>window.location.href='/chat_'</script>";
      }
      if (isset($_POST['c_d'])) {
        unset($_SESSION['debug']);
      }
		$sqlm = new SQL('chat_m');
    ?>
  </head>
  <body>
  	<div id='main_wrapper'>
      <div id='user_s'>
        <form action='' method='post' id='l_out' style='display:none'>
          <input class='l_btn' type='submit' name='log_out' value='Log out |'>
          <!--<input type='submit' name='c_d' value='clear_d'>-->
        </form>
      </div>
      <div id='user_list'><?php
		$res = mysqli_query($sqlm->link, "select distinct src from chat_m where dst = '$_SESSION[name]' and src <> '$_SESSION[name]'");
		$msga = $msga_t = array();
		while ($row = mysqli_fetch_row($res))
            array_push($msga, $row[0]);
		$res = mysqli_query($sqlm->link, "select distinct dst from chat_m where src = '$_SESSION[name]' and dst <> '$_SESSION[name]'");
        while ($row = mysqli_fetch_row($res))
            array_push($msga_t, $row[0]);
		$msga = array_merge(array_intersect($msga, $msga_t), array_diff($msga, $msga_t));
        echo "<div id='alias'>";
        foreach ($msga as $value) {
          echo "<div class='to'><div class='to_c'></div><div class='to_n' onclick='load(this.innerHTML, 0)'>$value</div></div>";
        }
		echo "</div>";
		echo "---------------------------------------------<br>";
		$users = mysqli_query($sqlm->link, "select login from chat_u where 1");
        while ($row = mysqli_fetch_row($users))
             echo "<div class='to'><div class='to_c'></div><div class='to_n' onclick='load(this.innerHTML, 0)'>$row[0]</div></div>";
        ?></div>
      <div id='msg_wrapper'>
        <div id='del'><input type='button' value='delete' onclick='dlt()'></div>
        <div id='l_bg'><img src='img/loading.gif' /></div>
        <div id='msg_box'></div>
        <textarea type='text' name='msg_inp' placeholder='message text' class='inp' onkeyup='enter(event)' onfocus='read()'></textarea>
        <input type='button' onclick='send()' value='send' class='snd_btn'/>
      </div>
    </div>
    <script src='js/jq-lib.js'></script>
    <script src='js/script.js'></script>
  </body>
</html>